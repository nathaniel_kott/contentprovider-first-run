package com.natekott.weathercontentprovider;

import android.database.Cursor;

public class Weather {
	private long id;
	private int temp;
	private String date;
	
	/**
	 * Default Constructor
	 */
	public Weather() {}
	
	/**
	 * Creates a weather object from a db cursor
	 * @param cursor
	 */
	public Weather(Cursor cursor){
			this.id = cursor.getLong(0);
			this.temp = cursor.getInt(1);
			this.date = cursor.getString(2);
	}
	
	////////////////////////
	// Getters and Setters//
	////////////////////////
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getTemp() {
		return temp;
	}
	public void setTemp(int temp) {
		this.temp = temp;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Appends all fields into a comma separated list
	 */
	public String toString(){
		StringBuilder weatherString = new StringBuilder();
		weatherString.append(this.id).append(", ").append(this.temp).append(", ").append(this.date).append("\n");
		return weatherString.toString();
		
	}
}
