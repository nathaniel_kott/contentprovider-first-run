package com.natekott.weathercontentprovider;
import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * http://www.coderzheaven.com/2013/09/22/write-custom-content-provider-android-sample-application-custom-content-provider/
 * 
 * You can think of a content provider like an API that lives locally inside your device,
 * other things access it via URIs and it makes changes to the database or returns data
 * based on which URI it received a request on
 * @author loki
 *
 */
public class WeatherContentProvider extends ContentProvider {

	
	/**
	 * The Uri_Matcher is a convenience class that maps your potential uris
	 * to integers so that they can be easily switched
	 */
	private static final UriMatcher URI_MATCHER;
	
	/*
	 * These are the integers
	 */
	private static final int WEATHER_ALL = 1;
	private static final int WEATHER_ONE = 2;
	
	/*
	 * Here we declare what path matches up with which int
	 */
	static{
		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		// will match /weather
		URI_MATCHER.addURI(WeatherMetaData.AUTHORITY, "weather", WEATHER_ALL);
		
		// will match /weater/#   (id)
		URI_MATCHER.addURI(WeatherMetaData.AUTHORITY, "weather/#", WEATHER_ONE);
	}
	
	
	//not sure why we need to do this, seems trivial
	private static final HashMap<String, String> sWeatherColumnProjectionMap;
	static{
		sWeatherColumnProjectionMap = new HashMap<String, String>();
		sWeatherColumnProjectionMap.put(WeatherMetaData.WeatherTable.ID, WeatherMetaData.WeatherTable.ID);
		sWeatherColumnProjectionMap.put(WeatherMetaData.WeatherTable.TEMP, WeatherMetaData.WeatherTable.TEMP);
		sWeatherColumnProjectionMap.put(WeatherMetaData.WeatherTable.DATE, WeatherMetaData.WeatherTable.DATE);
		
	}
	
	/**
	 * Gets us the connection to the database
	 */
	private WeatherSQLiteHelper mWeatherSQLiteHelper;
	
	@Override
	public boolean onCreate() {
		mWeatherSQLiteHelper = new WeatherSQLiteHelper(getContext());
		return false;
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * determines the type of uri MIME type of the content that we will be returning
	 * 
	 * Pretty sure I made up a type in WeatherMetaData ????
	 */
	@Override
	public String getType(Uri uri) {
		switch(URI_MATCHER.match(uri)){
			case WEATHER_ALL:
				return WeatherMetaData.CONTENT_TYPE_WEATHER_ALL;
			case WEATHER_ONE:
				return WeatherMetaData.CONTENT_TYPE_WEATHER_ONE;
			default:
				throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		
	}

	/**
	 * Inserts into the database
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		if(URI_MATCHER.match(uri) != WEATHER_ONE){
			throw new IllegalArgumentException(" Unkown URI: " + uri);
		}
		
		//Insert one row
		SQLiteDatabase db = mWeatherSQLiteHelper.getWritableDatabase();
		long rowId = db.insert(WeatherMetaData.WeatherTable.TABLE_NAME, null, values);
		
		if(rowId > 0){
			Uri weatherUri = ContentUris.withAppendedId(WeatherMetaData.CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(weatherUri, null);
			return weatherUri;
		}
		throw new IllegalArgumentException("<Illegal>Unknown URI: " + uri);
		
	}

	

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		
		switch(URI_MATCHER.match(uri)){
			case WEATHER_ALL:
				builder.setTables(WeatherMetaData.WeatherTable.TABLE_NAME);
				builder.setProjectionMap(sWeatherColumnProjectionMap);
				break;
			case WEATHER_ONE:
				builder.setTables(WeatherMetaData.WeatherTable.TABLE_NAME);
				builder.setProjectionMap(sWeatherColumnProjectionMap);
				builder.appendWhere(WeatherMetaData.WeatherTable.ID + " = " + uri.getLastPathSegment());
				break;
			default:
				throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		SQLiteDatabase db = mWeatherSQLiteHelper.getReadableDatabase();
		Cursor queryCursor = builder.query(db, 
										   projection, 
										   selection, 
										   selectionArgs, 
										   null, 
										   null, 
										   null);
		
		queryCursor.setNotificationUri(getContext().getContentResolver(), uri);
		return queryCursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
