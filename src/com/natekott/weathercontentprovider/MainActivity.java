package com.natekott.weathercontentprovider;

import com.example.testcontentprovider.R;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private WeatherDAO weatherDAO; 
	TextView weatherList;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        
        Cursor cur = getContentResolver().query(Uri.withAppendedPath(WeatherMetaData.CONTENT_URI, "weather"), null, null, null, null);
        if(cur.getCount() > 0){
        	Log.i("Main", "Showing Values....");
        	while(cur.moveToNext()){
        		long id = cur.getLong(0);
        		int temp = cur.getInt(1);
        		String date = cur.getString(2);
        		
        		System.out.println("Id = " + id + ", Temp : " + temp+ ", Date : " + date);
        	}
        	makeToast("Check LogCat");
        	
        }else{
        	Log.i("Main", "No Notes added");
            makeToast("No Notes added");
        }
     
        
        
    }
    private void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
