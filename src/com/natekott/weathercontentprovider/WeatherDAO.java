package com.natekott.weathercontentprovider;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


/**
 * Handles the database input and output
 * could otentially abstract all of these into a single "entry point"
 * @author loki
 *
 */
public class WeatherDAO {
	
	private SQLiteDatabase weatherDatabase;
	
	//the SQLiteHelper is responsible for creating the database and tables,
	//it also returns the SQLiteDatabase
	private WeatherSQLiteHelper weatherDBHelper;
	
	private String[] allColumns = {
								   WeatherSQLiteHelper.COLUMN_ID,
								   WeatherSQLiteHelper.COLUMN_TEMP,
								   WeatherSQLiteHelper.COLUMN_DATE
								   };
	
	
	public WeatherDAO(Context context){
		weatherDBHelper = new WeatherSQLiteHelper(context);
	}
	
	public void open() throws SQLException {
		weatherDatabase = weatherDBHelper.getWritableDatabase();
	}
	
	public void close(){
		weatherDBHelper.close();
	}
	
	/**
	 * Inserts a new weather row in to the table 
	 * @param temp the temperature
	 * @param date a string that represents the date
	 * @return the weather object that was just inserted
	 */
	public Weather createWeather(int temp, String date){
		ContentValues values = new ContentValues();
		values.put(WeatherSQLiteHelper.COLUMN_TEMP, temp);
		values.put(WeatherSQLiteHelper.COLUMN_DATE, date);
		long insertId = weatherDatabase.insert(WeatherSQLiteHelper.TABLE_WEATHER,
											 null,
											 values);
		Cursor cursor = weatherDatabase.query(WeatherSQLiteHelper.TABLE_WEATHER, 
											  allColumns, 
											  WeatherSQLiteHelper.COLUMN_ID + " = " + insertId, 
											  null, 
											  null, 
											  null, 
											  null);
		cursor.moveToFirst();
		Weather weather = new Weather(cursor);
		cursor.close();
		return weather;
		
	}
	
	/**
	 * Gets all of the rows from the table
	 * @return a list of all the Weather objects in the database 
	 */
	public List<Weather> getAllWeather(){
		List<Weather> weathers = new ArrayList<Weather>();
		
		Cursor cursor = weatherDatabase.query(WeatherSQLiteHelper.TABLE_WEATHER,
											  allColumns,
											  null,
											  null,
											  null,
											  null,
											  null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			Weather weather = new Weather(cursor);
			weathers.add(weather);
			cursor.moveToNext();
		}
		
		cursor.close();
		return weathers;
		
	}
	
	
	
}
