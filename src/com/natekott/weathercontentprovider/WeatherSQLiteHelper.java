package com.natekott.weathercontentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WeatherSQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_WEATHER = "weather";
	
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_TEMP = "temp";
	public static final String COLUMN_DATE = "date";
	
	private final static String DATABASE_CREATE = 
									"CREATE TABLE " + TABLE_WEATHER  + " (" +
															COLUMN_ID + " integer primary key autoincrement, " +
															COLUMN_TEMP + " integer not null, " +
															COLUMN_DATE + " date text not null)";
	
	public static final String DATABASE_NAME = "weather.db";
	public static final int DATABASE_VERSION = 1;
	
	 public WeatherSQLiteHelper(Context context) {
		    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	 }
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}
