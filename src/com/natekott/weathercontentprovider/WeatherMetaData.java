package com.natekott.weathercontentprovider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * A class that holds all of the data about our database schema in an easy to access java class
 * @author loki
 *
 */
public class WeatherMetaData {
	public WeatherMetaData(){}
	
	
	///////////////////////////
	// Content Provider Info //
	///////////////////////////
	
	//the name of you content provider (as declared in manifest)
	public static final String AUTHORITY = "com.natekott.weathercontentprovider";
	
	//the uri other applications will use to access your data
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
	
	
	
	
	
	///////////////////
	// DATABASE INFO //
	///////////////////
	
	//TODO: these are duplicate from WeatherSQLiteHelper
	public static final String DATABASE_NAME = "weather.db";
	public static final int DATABASE_VERSION = 1;
	
	public static final String CONTENT_TYPE_WEATHER_ALL = "vnd.android.cursor.dir/vnd.natekott.weather";
	public static final String CONTENT_TYPE_WEATHER_ONE = "vnd.android.cursor.item/vnd.natekott.weater";
	
	
	public class WeatherTable implements BaseColumns{
		private WeatherTable(){}
		
		public static final String TABLE_NAME = "weather";
		public static final String ID = "id";
		public static final String TEMP = "temp";
		public static final String DATE = "date";
	}
}
